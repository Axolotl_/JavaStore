public class Main {

	/*	Explanation:
	*		The purpose of this program is to prove that Java is a pass
	* 		by value language. Because Java passes object parameters
	* 		by value. The string that is passed to testFunction in this
	* 		example is just a copy of the original string that is changed
	* 		and printed in the scope of testFunction and then is discarded
	* 		where as the original testString is printed in main again.
	* 		Primitive types are allocated in the stack memory, so only the local
	* 		value will be changed.
	*/

	public static void main(String[] args) {
		// Set testString to initial value and print
		String testString = "Hello Jim!";
		System.out.printf("From main Function: %s\n", testString);

		// Call testFunction to try and change the state of testString
		testFunction(testString);

		// Printf prints the string "Hello Jim!" as testFunction has not changed
		// the value of testString in the calling function
		System.out.printf("From main Function Second Time: %s\n", testString);
	}

	// Changes the value of testString in the scope of testFunction
	private static void testFunction(String testString) {
		testString = "Hello Dave!";
		System.out.printf("From testFunction: %s\n", testString);
	}
}
