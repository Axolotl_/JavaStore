public class Main {
	public static void main(String[] args) {
		// Declare Variables
		int daysInAYear = 365;
		int daysInAWeek = 7;
		//int daysInAWeek = 5;
		int hoursInADay = 24;
		int minutesInAHour = 60;
		int secondsInAMinute = 60;
		int millisecondsInASecond = 1000;


		// Begin calculations
		int secondsInAHour = secondsInAMinute * minutesInAHour;
		int secondsInAYear = ((secondsInAHour * hoursInADay) * daysInAYear);

		System.out.println("Seconds in a hour: " + secondsInAHour);
		System.out.println("Seconds in a year: " + secondsInAYear);

		// Weeks in a year
		int weeksInAYear = (daysInAYear / daysInAWeek);
		System.out.println("Weeks in a year: " + weeksInAYear);

		// Hours in a year
		int hoursInAYear = ((daysInAYear - daysInAWeek) * hoursInADay);
		System.out.println("Hours in a year: " + hoursInAYear);

		// Milliseconds in a year
		// Typecast millisecondsInASecond to long
		long millisecondsInAYear = (secondsInAYear * (long) millisecondsInASecond);
		System.out.println("Milliseconds in a year: " + millisecondsInAYear);
	}
}
