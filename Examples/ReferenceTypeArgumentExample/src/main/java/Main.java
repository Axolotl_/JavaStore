public class Main {

	/*	Explanation:
	*		This program is designed to show that Java is all ways a pass by
	* 		value language, so when a reference type like an array is passed
	* 		by value to a called function that function can modify the value
	* 		of one of those array elements.
	*
	* 	Note For C Programmers:
	* 		Though the language used is different think of passing values to
	* 		methods in java like providing a pointer to the memory address
	* 		of an object in C.
	*/

	public static void main(String[] args) {
		// Set the initial value of testArray and print it
		String[] testArray = new String[] { "Hello ", "World" };
		printArray("Main Function: ", testArray);

		// Call testFunction to change the second element in testArray
		testFunction(testArray);

		// Print the array again to prove it has changed
		printArray("Main Function Call 2: ", testArray);
	}

	// Changes the value of the second element in testArray to see if it
	// changes in main
	private static void testFunction(String[] testArray) {
		// Change the second element of test array
		testArray[1] = "Pineapple";
		printArray("testFunction: ", testArray);
	}

	// Prints and formats an array with a prepended message
	private static void printArray(String pMsg, String[] pArray) {
		// Print prepended message
		System.out.printf("%s", pMsg);

		// Print all elements of a array
		for(String x : pArray) {
			System.out.printf("%s", x);
		}
		System.out.println();
	}
}
